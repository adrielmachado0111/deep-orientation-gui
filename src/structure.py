import argparse as ap
import os
import cv2
import matplotlib.pyplot as plt
from matplotlib.widgets import TextBox
import matplotlib.gridspec as gridspec
import numpy as np
import seaborn
from scipy.stats import norm
from scipy.stats import circmean, circstd
import utils.img as img_utils
from utils.io import get_files_by_extension
import tensorflow.keras.backend as K
from deep_orientation import beyer    # noqa # pylint: disable=unused-import
from deep_orientation import mobilenet_v2    # noqa # pylint: disable=unused-import
from deep_orientation import beyer_mod_relu     # noqa # pylint: disable=unused-import

from deep_orientation.inputs import INPUT_TYPES
from deep_orientation.inputs import INPUT_DEPTH, INPUT_RGB, INPUT_DEPTH_AND_RGB
from deep_orientation.outputs import OUTPUT_TYPES
from deep_orientation.outputs import (OUTPUT_REGRESSION, OUTPUT_CLASSIFICATION,
                                      OUTPUT_BITERNION)
import deep_orientation.preprocessing as pre
import deep_orientation.postprocessing as post



# seaborn.set_style('darkgrid')
seaborn.set_context('notebook', font_scale=1.2)

import time

#----------- Crea la red neuronal ------------------------------
def load_network(model_name, weights_filepath,
                 input_type, input_height, input_width,
                 output_type,
                 sampling=False,
                 **kwargs):

    # load model --------------------------------------------------------------
    model_module = globals()[model_name]
    model_kwargs = {}
    if model_name == 'mobilenet_v2' and 'mobilenet_v2_alpha' in kwargs:
        model_kwargs['alpha'] = kwargs.get('mobilenet_v2_alpha')
    if output_type == OUTPUT_CLASSIFICATION:
        assert 'n_classes' in kwargs
        model_kwargs['n_classes'] = kwargs.get('n_classes')

    model = model_module.get_model(input_type=input_type,
                                   input_shape=(input_height, input_width),
                                   output_type=output_type,
                                   sampling=sampling,
                                   **model_kwargs)

    # load weights ------------------------------------------------------------
    model.load_weights(weights_filepath)

    return model
#-----------------------------------------------------------------------------


# -------------------------------- define preprocessing function -------------------------------------------
def load_and_preprocess(inputs, input_type, input_height, input_width, input_preprocessing, n_samples):

    # unpack inputs
    if input_type == INPUT_DEPTH_AND_RGB:
        depth_filepath, rgb_filepath, mask_filepath = inputs
    elif input_type == INPUT_DEPTH:
        depth_filepath, mask_filepath = inputs
    else:
        rgb_filepath, mask_filepath = inputs

    # pack shape
    shape = (input_height, input_width)

    # load mask
    mask = img_utils.load(mask_filepath)
    mask_resized = pre.resize_mask(mask, shape)
    mask_resized = mask_resized > 0

    # prepare depth input
    if input_type in [INPUT_DEPTH, INPUT_DEPTH_AND_RGB]:
        # load
        depth = img_utils.load(depth_filepath)

        # create mask
        # mask = depth > 0
        # mask_resized = pre.resize_mask(mask.astype('uint8')*255, shape) > 0

        # mask (redundant, since mask is derived from depth image)
        # depth = pre.mask_img(depth, mask)

        # resize
        depth = pre.resize_depth_img(depth, shape)

        # 01 -> 01c
        depth = depth[..., None]

        # preprocess
        depth = pre.preprocess_img(
            depth,
            mask=mask_resized,
            scale01=input_preprocessing == 'scale01',
            standardize=input_preprocessing == 'standardize',
            zero_mean=True,
            unit_variance=True)

        # convert to correct data format
        if K.image_data_format() == 'channels_last':
            axes = 'b01c'
        else:
            axes = 'bc01'
        depth = img_utils.dimshuffle(depth, '01c', axes)

        # repeat if sampling is enabled
        if n_samples > 1:
            depth = np.repeat(depth, n_samples, axis=0)

    # prepare rgb input
    if input_type in [INPUT_RGB, INPUT_DEPTH_AND_RGB]:
        # load
        rgb = img_utils.load(rgb_filepath)

        # create mask
        # if args.input_type == INPUT_RGB:
        #     # derive mask from rgb image
        #     mask = rgb > 0
        #     mask_resized = pre.resize_mask(mask.astype('uint8')*255,
        #                                    shape) > 0
        # else:
        #     # mask rgb image using mask derived from depth image
        #    rgb = pre.mask_img(rgb, mask)

        # resize
        rgb = pre.resize_depth_img(rgb, shape)

        # preprocess
        rgb = pre.preprocess_img(
            rgb,
            mask=mask_resized,
            scale01=input_preprocessing == 'scale01',
            standardize=input_preprocessing == 'standardize',
            zero_mean=True,
            unit_variance=True)

        # convert to correct data format
        if K.image_data_format() == 'channels_last':
            axes = 'b01c'
        else:
            axes = 'bc01'
        rgb = img_utils.dimshuffle(rgb, '01c', axes)

        # repeat if sampling is enabled
        if n_samples > 1:
            rgb = np.repeat(rgb, n_samples, axis=0)

    # return preprocessed images
    if input_type == INPUT_DEPTH_AND_RGB:
        return depth, rgb
    elif input_type == INPUT_DEPTH:
        return depth,
    else:
        return rgb,

def postprocess(output, output_type):
    if output_type == OUTPUT_BITERNION:
        return post.biternion2deg(output)
    elif output_type == OUTPUT_REGRESSION:
        return post.rad2deg(output)
    else:
        return post.class2deg(np.argmax(output, axis=-1), n_classes)

""" print("file path: ->    "+str(filepaths[0]))
for i, inputs in enumerate(filepaths):

    #print("[{:0{}d}/{:0{}d}]: {}".format(i+1, len_cnt, len(filepaths),
    #                                    len_cnt, inputs))
    print(inputs)
    print(type(input))
    # load and preprocess inputs
    #nw_inputs = load_and_preprocess(inputs)

    # predict
    #nw_output = model.predict(nw_inputs, batch_size=n_samples)

    # postprocess output
    #output = postprocess(nw_output)
    #<< --------- Salida ----------------------------------------------------->>

    #print("-------- Salida ----> " + str(np.mean(output)))
#lis = (inputs[0], inputs[1])
#print(lis)
nw_inputs = load_and_preprocess(filepaths[0])
nw_output = model.predict(nw_inputs, batch_size=n_samples)
output = postprocess(nw_output)
 #<< --------- Salida ----------------------------------------------------->>
print("-------- Salida ----> " + str(np.mean(output))) """