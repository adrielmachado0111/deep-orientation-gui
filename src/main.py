""" ------------------- Deep Orientation GUI ------------------------
By: Adriel Machado Buelvas
By: Shirly Vanessa Morales
"""#----------------- LIB Graphic User Interface --------------------
from PyQt5.QtWidgets import*
from PyQt5.uic import loadUi
from PyQt5 import QtCore, QtGui, QtWidgets
from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)
from time import time, sleep
#start_time = time()
import numpy as np
from math import sqrt
#--------------------- Libs NeuralNetwork ----------------------------
import structure#Estructura principal de lewandowshy
import utils.img as img_utils
from utils.io import get_files_by_extension
import tensorflow.keras.backend as K
import os
from deep_orientation.inputs import INPUT_DEPTH, INPUT_RGB, INPUT_DEPTH_AND_RGB
#-------------------------------------------------------------------
"""----------------- Colors ----------------------------------------
yellow
rgb(212,212,0);
red
rgb(140,0,0);
green
rgb(56,111,0);
-----------------------------------------------------------------"""
class MatplotlibWidget(QMainWindow):
    
    def __init__(self):
        
        QMainWindow.__init__(self)
        #-> GUI version 2
        #loadUi("./config/gui-deep-orientation2.ui",self)
        #-> GUI version 3: añade el txt de la imagen actual
        loadUi("./config/gui-deep-orientation2.ui",self)

        self.setWindowTitle("Deep Orientation")
        self.progressBar.hide()
        self.btnCompute.clicked.connect(self.update_graph)
        self.toolButton.clicked.connect(self.find_path)
        self.Led_Green.show()
        self.Led_Green.setStyleSheet("background-color:rgb(229, 229, 229);border-radius: 25px;")
        self.Led_Yellow.hide()
        self.Led_Red.hide()
        #self.addToolBar(NavigationToolbar(self.MplWidget.canvas, self))
        #-------------- Constant's ----------------------------------------------
        self.image_or_image_basepath = "../nicr_rgb_d_orientation_data_set_examples/small_patches"
        self.CmbWeights.currentIndexChanged.connect(self.selectionchange)
        #final_time = time() - start_time
        #print("Tiempo estimado de apertura de la app: %.10f seconds." % final_time)

    def update_graph(self):
        #time_load = time()
        self.progressBar.show()
        #self.image.setPixmap(QtGui.QPixmap("../nicr_rgb_d_orientation_data_set_examples/small_patches/P1-312_RGB.png"))#'
        self.progressBar.setValue(0)
        model_type = "mobilenet_v2"
        weights_filepath = "../trained_networks/mobilenet_v2_1_00__rgb__96x96__biternion__0_001000__0/weights_valid_0134.hdf5"
        input_type = "rgb"
        input_width = 96
        input_height = 96
        input_preprocessing = "scale01"
        n_samples = 1
        output_type = "biternion"
        n_classes = 8
        mobilenet_v2_alpha = 1.0
        devices = "0"
        cpu = True
        verbose = False
        #--> set device and data format
        if cpu:
            os.environ['CUDA_VISIBLE_DEVICES'] = ''
            devices = ''
        else:
            os.environ['CUDA_VISIBLE_DEVICES'] = devices
        if not devices or model_type == 'mobilenet_v2':
            # note: tensorflow supports b01c pooling on cpu only
            K.set_image_data_format('channels_last')
        else:
            K.set_image_data_format('channels_first')
        #-> busqueda de archivos
        DEPTH_SUFFIX = '_Depth.pgm'
        RGB_SUFFIX = '_RGB.png'
        MASK_SUFFIX = '_Mask.png'
        #------------------- get filepaths [Lewandosky] -----------------------
        self.progressBar.setValue(8)
        mask_filepaths = get_files_by_extension(
                self.image_or_image_basepath, extension=MASK_SUFFIX.lower(),
                flat_structure=True, recursive=True, follow_links=True)

        if input_type in [INPUT_DEPTH, INPUT_DEPTH_AND_RGB]:
            depth_filepaths = get_files_by_extension(
                self.image_or_image_basepath, extension=DEPTH_SUFFIX.lower(),
                flat_structure=True, recursive=True, follow_links=True)
            assert len(depth_filepaths) == len(mask_filepaths)
            filepaths = list(zip(depth_filepaths, mask_filepaths))
            assert all(depth_fp.replace(DEPTH_SUFFIX, '') ==
                        mask_fp.replace(MASK_SUFFIX, '')
                        for depth_fp, mask_fp in filepaths)

        if input_type in [INPUT_RGB, INPUT_DEPTH_AND_RGB]:
            rgb_filepaths = get_files_by_extension(
                self.image_or_image_basepath, extension=RGB_SUFFIX.lower(),
                flat_structure=True, recursive=True, follow_links=True)
            assert len(rgb_filepaths) == len(mask_filepaths)
            filepaths = list(zip(rgb_filepaths, mask_filepaths))
            assert all(rgb_fp.replace(RGB_SUFFIX, '') ==
                        mask_fp.replace(MASK_SUFFIX, '')
                        for rgb_fp, mask_fp in filepaths)

        if input_type == INPUT_DEPTH_AND_RGB:
            filepaths = list(zip(depth_filepaths, rgb_filepaths, mask_filepaths))
        
        self.progressBar.setValue(10)
        # --------------------- load model --------------------------------------------------------------
        model = structure.load_network(model_type, weights_filepath, input_type, input_height, input_width, output_type, sampling=n_samples > 1, n_classes=n_classes, mobilenet_v2_alpha=mobilenet_v2_alpha)
        #------------------ Tiempo de carga de caracteristicas ------------
        #final_time_load = time() - time_load
        #print("Tiempo estimado de carga de caracteristicas: %.10f seconds." % final_time_load)

        
        final_time_predict = 0
        for i, inputs in enumerate(filepaths):#recorre uno a uno las imagenes y mascaras
            #-> tiempo de prediccion de angulo y calculo de distancia por imagen
            #time_predict = time()
            #sleep(0.5)# retardo de 0.5seg
            if i >= 80:
                self.progressBar.setValue(90) 
            else: 
                self.progressBar.setValue(10+i)
            #---> carga y preprocesa las imagenes [lewandosky]
            nw_inputs = structure.load_and_preprocess(inputs, input_type, input_height, input_width, input_preprocessing, n_samples)
            #-----> prediccion                   {Lewandosky}
            nw_output = model.predict(nw_inputs, batch_size=n_samples)
            #----> configura la salida          {Lewandosky}
            output = structure.postprocess(nw_output, output_type)
            
            self.image.setPixmap(QtGui.QPixmap(inputs[0]))#-> mostramos la imagen en el label (image)
            self.MplWidget.canvas.axes.clear()
            self.MplWidget.canvas.axes.hist(np.deg2rad(output), width=np.deg2rad(5), density = True, alpha=1.0, color='#b62708')
            self.MplWidget.canvas.axes.set_theta_zero_location('S', offset=0)
            self.MplWidget.canvas.axes.set_yscale('symlog')
            self.MplWidget.canvas.axes.set_ylim([0, 20])
            self.MplWidget.canvas.draw()#->  estimar el calculo de la distancia
            #-------> calculamos la distancia e ingresamos como parametro el nombre de la imagen como: inputs[0]-> ./RGB_P1.png | inputs[1]-> ./mask_P1.png
            distance, dist_per_cam, deg = self.import_find_values(inputs[0])#distance: dis persona a robot.
            #distance = self.calculate_distance(inputs[0])
            #-------> calculamos el error = grados reales - grados inferidos
            error = abs(deg - output)[0]
            print("Error: "+ str(int(error)))
            #print("Distance: "+str(x)+" Output: " + str(output))
            self.txtDistance.setText(str(distance) + " m")
            self.txtError.setText(str(int(error)))
            #---------> mostrar imagen actual en el txt ---------------------
            if len(inputs[0]) == 70:# si el numero es un solo digito ejemp: RGB_P1.png
                num = int(inputs[0][61: 62])
            elif len(inputs[0]) == 71:# 2 digitos ejemp: RGB_P10.png
                num = int(inputs[0][61: 63])
            elif len(inputs[0]) == 72:# 3 digitos ejemp: RGB_P100.png
                num = int(inputs[0][61: 64])
            #self.txtImage.setText(str(num))
            #f = random.randint(180, 270)
            if ((distance > 1.02 and distance <= 2.5) and (output > 91 and output < 359)) or distance > 2.5:#si la distancia esta entre 3.5 y 2.5 metros
                self.Led_Green.show()
                self.Led_Green.setStyleSheet("background-color:rgb(56,111,0);border-radius: 25px;")
                self.Led_Yellow.hide()
                self.Led_Red.hide()
                self.txtIndicator.setText("RUN!")
            elif distance > 1.02 and distance <= 2.5 and (output > 0 and output < 90):#segun la norma ISO/TS: baliza amarilla a 1.190m -> velocidad al 50%
                self.Led_Yellow.show()
                self.Led_Green.setStyleSheet("background-color:rgb(212,212,0);border-radius: 25px;")
                self.Led_Green.hide()
                self.Led_Red.hide()
                self.txtIndicator.setText("Warning!")
            elif (distance > 0.8 and distance <= 1.02) and (output > 0 and output < 90):# enciende la baliza a 1.020m y la orientacion esta entre 0 y 90 -Z velocidad al 25%
                self.Led_Red.show()
                self.Led_Green.setStyleSheet("background-color:rgb(140,0,0);border-radius: 25px;")
                self.Led_Green.hide()
                self.Led_Yellow.hide()
                self.txtIndicator.setText("STOP!")
            elif distance < 0.9:  
                self.Led_Red.show()
                self.Led_Green.setStyleSheet("background-color:rgb(140,0,0);border-radius: 25px;")
                self.Led_Green.hide()
                self.Led_Yellow.hide()
                self.txtIndicator.setText("STOP!")
            #final_time_predict += time() - time_predict#acumulador de tiempos
            #print("Tiempo estimado prediccion y estimacion por imagen: %.10f seconds." % final_time_predict)
        #print("tiempo promedio de prediccion y calculo de distancia por imagen: " + str(final_time_predict/i))#suma de los tiempos / numero de muestras
            # elif x > 1.02 and x <= 2.5:
            #     self.Led_Yellow.show()
            #     self.Led_Green.hide()
            #     self.Led_Red.hide()
            #     self.txtIndicator.setText("Warning!")
        
        self.progressBar.setValue(100)
        self.progressBar.hide()
        #self.MplWidget.canvas.axes.clear()
        #self.MplWidget.canvas.axes.plot(t, cosinus_signal)
        #self.MplWidget.canvas.axes.plot(t, sinus_signal)
        #self.MplWidget.canvas.axes.hist(np.deg2rad(f), width=np.deg2rad(5), density = True, alpha=0.5, color='#b62708')
        #self.MplWidget.canvas.axes.legend(('cosinus', 'sinus'),loc='upper right')
        #self.MplWidget.canvas.axes.set_title('Cosinus - Sinus Signal')
        #self.MplWidget.canvas.draw()

    def calculate_distance(self, input_path):#-> Calcular distancia
        num=0
        calc_distance=0
        #print(input_path)
        #----> pre-process data input from Graph User Interface GUI
        if len(input_path) == 70:# si el numero es un solo digito ejemp: RGB_P1.png
            num = int(input_path[61: 62])
        elif len(input_path) == 71:# 2 digitos ejemp: RGB_P10.png
            num = int(input_path[61: 63])
        elif len(input_path) == 72:# 3 digitos ejemp: RGB_P100.png
            num = int(input_path[61: 64])
        #------------- find image rectangle ----------
        data = self.import_data()#importamos las caracteristicas del archivo de text
        for i in range(data.shape[1]):#recorrer todo la database
            num_img = data[0][i]#el numero de identificacion (int) de la imagen en ese momento [RGB1, RGB2..]
            if num_img == num: #si el numero de la imagen del archivo de text es igual al num de la img actual de la predict
                #print("RGB"+str(num_img))
                Dxp = (data[2][i] + (data[4][i])/2)# distacia en x + width/2 -> [pixeles]
                Dxm = ((4.48)*(Dxp)/640)# Regla de 3: 4.48m -> 640px: [metros]
                calc_distance = sqrt((3.36 - Dxm)**2 + (1.750 - data[6][i])**2)# calculamos la distancia con un pitagorazo
                i=0
                break
        return round(calc_distance, 3)#solo tomamos las 3 cifras significativas
    #----- Import data ----------
    def import_data(self):
        dataset = np.loadtxt("./config/data_base.txt", delimiter=',')
        X = (np.array([dataset[:, 0], dataset[:, 1], dataset[:, 2], dataset[:, 3], dataset[:, 4], dataset[:, 5], dataset[:, 6]]))
        return X# Nombre_imagen | 1 | coor_x | coor_y | width | height | depth
    
    #------ Find and import values from txt for 
    def import_find_values(self, input_path):
        dataset = np.loadtxt("./config/Valores_F.txt", delimiter=',')
        X = (np.array([dataset[:, 0], dataset[:, 1], dataset[:, 2], dataset[:, 3]]))
        num=0
        #print(input_path)
        #----> pre-process data input from Graph User Interface GUI
        if len(input_path) == 70:# si el numero es un solo digito ejemp: RGB_P1.png
            num = int(input_path[61: 62])
        elif len(input_path) == 71:# 2 digitos ejemp: RGB_P10.png
            num = int(input_path[61: 63])
        elif len(input_path) == 72:# 3 digitos ejemp: RGB_P100.png
            num = int(input_path[61: 64])
        #------------- find image rectangle ----------
        for i in range(X.shape[1]):#recorrer todo la database
            num_img = X[0][i]#el numero de identificacion (int) de la imagen en ese momento [RGB1, RGB2..]
            if num_img == num: #si el numero de la imagen del archivo de text es igual al num de la img actual de la predict
                

                return (X[1][i], X[2][i], X[3][i])# return-> d_persona_robot | d_persona_camara | grado_orientacion
                i=0
                break
                 
                
        

    def find_path(self):
        self.image_or_image_basepath = QFileDialog.getExistingDirectory( self, "Choose DataSet path", os.getcwd(), QFileDialog.ShowDirsOnly)
        
    def selectionchange(self, i):#-> funcion combo box
        if i == 0:
            #-> cuando el index es 0, entonces modifique los valores
            self.txtModel.setText("Beyer modified")
            self.txtInput.setText("Depth")
            self.txtOutput.setText("Biternion")
        elif i == 1:
            #-> cuando el index es 1, entonces modifique los valores
            self.txtModel.setText("Mobilenet_V2")
            self.txtInput.setText("Depth")
            self.txtOutput.setText("Biternion")
        elif i == 2:
            #-> cuando el index es 2, entonces modifique los valores
            self.txtModel.setText("Mobilenet_V2")
            self.txtInput.setText("RGB")
            self.txtOutput.setText("Biternion")

app = QApplication([])
window = MatplotlibWidget()
window.show()
app.exec_()
