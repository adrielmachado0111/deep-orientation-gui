# Deep Orientation GUI
## Orientation Estimate

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Presentations
![gif](/uploads/61b431ce8c460d76f9f768a94f47915d/gif.gif)


## Requirements

Libraries necessary for proper operation:

- python 3.6.*
- pyqt5
- pyqt5-tools
- opencv-python==3.4.3.*
- tensorflow==1.12.3
- tqdm
- scipy
- seaborn
- scikit-learn
- matplotlib
- seaborn


## Run

To run the Deep Orientation GUI program, go to the / src folder and type the following command in console:

```sh
python main.py
```


## License

MIT

**Free Software, Hell Yeah!**
